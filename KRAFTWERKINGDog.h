//
//  KRAFTWERKINGDog.h
//  Man's Best Friend
//
//  Created by RJ Militante on 1/6/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KRAFTWERKINGDog : NSObject

@property (nonatomic) int age;
@property (nonatomic, strong) NSString *breed;
@property (strong, nonatomic) UIImage *image;
@property (nonatomic, strong) NSString *name;

-(void)bark;
-(void)barkANumberOfTimes:(int)numberOfTimes;
-(void)changeBreedToWerewolf;
-(void)barkANumberOfTimes:(int)numberOfTimes loudly:(BOOL)isLoud;

-(int)ageInDogYearsFromAge:(int)realAge;

@end
