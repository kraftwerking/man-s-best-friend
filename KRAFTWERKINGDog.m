//
//  KRAFTWERKINGDog.m
//  Man's Best Friend
//
//  Created by RJ Militante on 1/6/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "KRAFTWERKINGDog.h"

@implementation KRAFTWERKINGDog

-(void)bark{
    
    NSLog(@"Woof Woof!");
    
};

-(void)barkANumberOfTimes:(int)numberOfTimes
{
    
    for(int bark = 1; bark <= numberOfTimes; bark++){
        [self bark];
    }
};

-(void)changeBreedToWerewolf
{
    self.breed = @"Werewolf";
};

-(void)barkANumberOfTimes:(int)numberOfTimes loudly:(BOOL)isLoud
{
    if(!isLoud){
        for(int bark = 1; bark <= numberOfTimes; bark++){
            NSLog(@"yip yip");
        }
    } else {
        for(int bark = 1; bark <= numberOfTimes; bark++){
            NSLog(@"WOOF WOOF");
        }
    }
};

-(int)ageInDogYearsFromAge:(int)realAge
{
    int newAge = realAge * 7;
    //NSLog(@"age in dog years is %i", newAge);
    return newAge;
};

@end
