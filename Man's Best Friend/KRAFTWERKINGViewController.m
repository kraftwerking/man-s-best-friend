//
//  KRAFTWERKINGViewController.m
//  Man's Best Friend
//
//  Created by RJ Militante on 1/6/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "KRAFTWERKINGViewController.h"
#import "KRAFTWERKINGDog.h"

@interface KRAFTWERKINGViewController ()

@end

@implementation KRAFTWERKINGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    KRAFTWERKINGDog *myDog  = [[KRAFTWERKINGDog alloc] init];
    myDog.name = @"Nana";
    myDog.breed = @"St. Bernard";
    myDog.age = 1;
    NSLog(@"My dog is named %@ and its age is %i and its breed is %@", myDog.name, myDog.age, myDog.breed);
    //myDog = nil;
    //NSLog(@"My dog is named %@ and its age is %i and its breed is %@", myDog.name, myDog.age, myDog.breed);
    [myDog bark];
    [myDog barkANumberOfTimes:11];
    NSLog(@"%@", myDog.breed);
    [myDog changeBreedToWerewolf];
    NSLog(@"%@", myDog.breed);
    
    [self printHey];
    
    [myDog barkANumberOfTimes:5 loudly:NO];
    [myDog barkANumberOfTimes:5 loudly:YES];
    
    //int dogYears = [myDog ageInDogYearsFromAge:1];
    NSLog(@"age in dog years from age %i", [myDog ageInDogYearsFromAge:1]);
    [self countDownToOne:14];
    [self countDownToNumber:14 num2:3];
    NSLog(@"factoral of 14 %i", [self factoral:14]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)printHey
{
    NSLog(@"Hey");
};

-(void)countDownToOne:(int)num
{
    if(num ==1){
        NSLog(@"%i", num);
    } else {
        for(int i = num; i > 1; i--){
        NSLog(@"%i", i);
        }
    }
    
};

-(void)countDownToNumber:(int)num num2:(int)num2
{

    int largerNum = num;
    int smallerNum = num2;
    
    if(num2 > num){
        largerNum = num2;
        smallerNum = num;
    }
    
    for(int i = largerNum; i > smallerNum; i--){
        NSLog(@"%i", i);
    }
    
};

-(int)factoral:(int)num
{
    int total = 0;
    if(num > 0) {
        for(int i = 1; i < num; i++){
            total = total + i;
        }
        return total;
    } else {
        return 1;
    }
};


@end
