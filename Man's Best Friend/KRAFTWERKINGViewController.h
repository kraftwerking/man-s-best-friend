//
//  KRAFTWERKINGViewController.h
//  Man's Best Friend
//
//  Created by RJ Militante on 1/6/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KRAFTWERKINGViewController : UIViewController

-(void)printHey;
-(void)countDownToOne:(int)num;
-(void)countDownToNumber:(int)num num2:(int)num2;
-(int)factoral:(int)num;

@end
